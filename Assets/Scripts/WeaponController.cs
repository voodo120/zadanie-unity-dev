using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public struct Weapon
{
    public enum Weapons
    {
        Pistol = 0,
        MachineGun = 1,
        Grenade = 2,
    }

    public Weapons choice;

}

public class WeaponController : MonoBehaviour
{

    GunSelector selectedWeapon;

    public GameObject grenadePrefab;
    public GameObject bulletPrefab;

    public GameObject throwPoint;

    public float throwForce;

    public Image weaponCrosshair;

    public Animation anim;

    public bool isWeaponReady = false;

    public float fireRate;
    float lastFire;

	void Update()
    {
        if (Input.GetMouseButton(0))
        {
            Fire(selectedWeapon);
        }
    }



    void Fire(GunSelector weapon)
	{
        anim = gameObject.GetComponent<Animation>();

        if (weapon == null)
        {
            weapon = GetComponentInParent<GunSelector>();
        }
        if (isWeaponReady || weapon.currentWeapon.choice == Weapon.Weapons.Grenade)
        {
            switch (weapon.currentWeapon.choice)
            {
                case Weapon.Weapons.Pistol:
                    if (Time.time - lastFire > 1 / fireRate)
				    {
                        lastFire = Time.time;
					    anim.Play("PistolGun_Fire_anim");
                        GameObject bullet = Instantiate(bulletPrefab, throwPoint.gameObject.transform.position, throwPoint.gameObject.transform.rotation);
                        bullet.AddComponent<Rigidbody>().velocity = throwPoint.transform.forward * throwForce;
                    }

                    break;
                case Weapon.Weapons.MachineGun:
                    if (Time.time - lastFire > 1 / fireRate)
                    {
                        lastFire = Time.time;
                        anim.Play("MachineGun_Fire_anim");
                        GameObject bulletMachineGun = Instantiate(bulletPrefab, throwPoint.gameObject.transform.position, throwPoint.gameObject.transform.rotation);
                        bulletMachineGun.AddComponent<Rigidbody>().velocity = throwPoint.transform.forward * throwForce;
                    }

                    break;
                case Weapon.Weapons.Grenade:
                    if (Time.time - lastFire > 1 / fireRate)
                    {
                        lastFire = Time.time;
                        if (weapon.grenadeAmmo > 0)
                        {
                            lastFire = Time.time;
                            ThrowGrenade(weapon);
                        }
                        else
                        {
                            weapon.ChangeWeapon();
                        }
                    }
                    break;
            }
        }

    }

    void ThrowGrenade(GunSelector weapon)
    {
        if (weapon.grenadeAmmo > 0)
        {
            GameObject grenadeObject = Instantiate(grenadePrefab, throwPoint.gameObject.transform.position, throwPoint.gameObject.transform.rotation);
            grenadeObject.AddComponent<Rigidbody>().velocity = throwPoint.transform.forward * throwForce;
            weapon.grenadeAmmo--;
        }
    }

}

