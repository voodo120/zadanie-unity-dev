using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeController : MonoBehaviour
{
    public ParticleSystem explosion;
	public GameObject grenadeTextures;

	[SerializeField]
	float explosionRange;

	private void OnCollisionEnter(Collision collision)
	{
		if (collision.gameObject.tag == "Ground")
		{
			
			explosion.gameObject.SetActive(true);

			Collider[] collisions = Physics.OverlapSphere(transform.position, explosionRange);
			foreach (Collider coll in collisions)
			{
				if (coll.tag == "Enemy")
				{
					Destroy(coll.gameObject);
				}
			}

			Destroy(grenadeTextures);
			Destroy(gameObject,0.5f);
		}
	}

}
