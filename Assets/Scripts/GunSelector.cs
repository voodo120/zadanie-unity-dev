using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GunSelector : MonoBehaviour
{

    private static GunSelector _instance;

    public static GunSelector Instance { get { return _instance; } }


    public Weapon currentWeapon;

    public WeaponController[] weaponBag;

    public int selectedWeaponIndex = 0;

    public int grenadeAmmo = 3;

    public PistolManager Pistol;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }
	void Start()
    {
        SelectWeapon();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("q")) {
            ChangeWeapon();
        }
    }

    void SelectWeapon()
    {
        weaponBag = GetComponentsInChildren<WeaponController>();

        foreach (var ob in weaponBag)
        {
            ob.gameObject.SetActive(false);
        }
        weaponBag[selectedWeaponIndex].gameObject.SetActive(true);
    }

    public void ChangeWeapon()
    {
        if (selectedWeaponIndex == (int)Weapon.Weapons.Pistol && Pistol.isZoomed)
        {
            Pistol.ToggleZoom(Pistol.isZoomed);
        }

        weaponBag[selectedWeaponIndex].gameObject.SetActive(false);
        selectedWeaponIndex++;


        if ((grenadeAmmo == 0 && selectedWeaponIndex == (int)Weapon.Weapons.Grenade) || selectedWeaponIndex == weaponBag.Length)
        {
            selectedWeaponIndex = 0;
        }

        
        weaponBag[selectedWeaponIndex].gameObject.SetActive(true);

        currentWeapon.choice = (Weapon.Weapons)selectedWeaponIndex;

        Debug.Log("Selected weapon is:" + currentWeapon.choice);

    }
}
