using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{
    public enum Enemies
    {
        Meele = 0,
        Distance = 1,
    }
    [SerializeField]
    public Enemies enemy;

    //unique kill only by pistol
    public bool isUnique;

    public GameObject[] weaponsToLoad;

    public PlayerController player;

    private NavMeshAgent agent;

    [SerializeField]
    float attackRange;

    [SerializeField]
    float chaseRange;

    [SerializeField]
    float attackSpeed;

    float lastAttack;


    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<PlayerController>();
        switch (enemy)
        {
            case Enemies.Meele:
                weaponsToLoad[(int)Enemies.Meele].SetActive(true);
                break;

            case Enemies.Distance:
                weaponsToLoad[(int)Enemies.Distance].SetActive(true);
                break;
        }

        agent = GetComponent<NavMeshAgent>();
        lastAttack = Time.deltaTime;


    }

    // Update is called once per frame
    void Update()
    {
        if (player != null)
		{
            ChasePlayer();
		}
        
    }

    void ChasePlayer()
	{

        float distance = Vector3.Distance(gameObject.transform.position, player.transform.position);
		switch (this.enemy)
		{
            case Enemies.Meele:
                if (distance >= chaseRange)
                {
                    agent.enabled = true;
                    agent.destination = player.transform.position;
                }
                else
                {
                    if (Time.time - lastAttack > 1 / attackSpeed)
                    {
                        lastAttack = Time.time;
                        agent.enabled = false;
                        AttackPlayer();
                    }

                }
				break;

            case Enemies.Distance:
                if (distance > chaseRange)
                {
                    agent.enabled = true;
                    agent.destination = player.transform.position;
                }
                else
                {
                    if (Time.time - lastAttack > 1 / attackSpeed)
                    {
                        lastAttack = Time.time;
                        agent.enabled = false;
                        AttackPlayer();
                    }

                }
                break;
		}
	}

    //if player is in enough range start shooting if ure shooting enemy
    void AttackPlayer()
	{
        // calculate object are in range of collider
        Collider[] coll = Physics.OverlapSphere(transform.position, attackRange);
		switch (this.enemy)
		{
            case Enemies.Meele:
                foreach (Collider collision in coll)
                {
                    if (collision.tag == "Player")
                    {
                        transform.LookAt(PlayerController.Instance.transform);
                        PlayerHealthManager.Instance.TakeDamage(1);
                    }
                }
                break;
            case Enemies.Distance:
                foreach (Collider collision in coll)
				{
                    if (collision.tag == "Player")
					{
                        transform.LookAt(PlayerController.Instance.transform);
                        PlayerHealthManager.Instance.TakeDamage(1);
                        //start shooting at player
					}
				}
                break;
		}
	}

    public void TakeDamage()
	{
        Destroy(gameObject);
	}
	private void OnDestroy()
	{
        GameController.Instance.deathEnemies += 1;
        GameController.Instance.scoreText.text = "Score : " + GameController.Instance.deathEnemies;
    }
}
