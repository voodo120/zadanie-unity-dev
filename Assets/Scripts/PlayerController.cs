using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    private static PlayerController _instance;
    public static PlayerController Instance { get { return _instance; } }

    [SerializeField]
    Camera playerCamera;

    public CharacterController controller;
    private Vector3 playerVelocity;
    private bool groundedPlayer;
    private float playerSpeed = 10.0f;
    private float gravityValue = -9.81f;



    private void Awake()
	{
        if (_instance != null && _instance != this)
		{
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }
	private void Start()
	{
        controller = GetComponent<CharacterController>();
        Teleport();
    }

    void Update()
    {
        CharacterMovement();
    }

    public void Teleport()
    {
        StartCoroutine("teleportDelay");
    }

    IEnumerator teleportDelay()
	{
        controller.enabled = false;
        gameObject.transform.position = new Vector3(0, 15, 0);
        yield return new WaitForSeconds(0.5f);
        controller.enabled = true;
    }

    void CharacterMovement()
	{

        groundedPlayer = controller.isGrounded;
        if (groundedPlayer && playerVelocity.y < 0)
        {
            playerVelocity.y = 0f;
        }

        Vector3 move = transform.TransformDirection(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));


        controller.Move(move * Time.deltaTime * playerSpeed);

        playerVelocity.y += gravityValue * Time.deltaTime;

        controller.Move(playerVelocity * Time.deltaTime);
        
    }

}

