using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
	private static GameController _instance;

	public static GameController Instance { get { return _instance; } }

	public EnemyController[] enemies;

	public Transform[] spawnPoints;

	public int totalEnemyCount;

	public int howManyUniques;

	public int deathEnemies;

	public GameObject youWinText;
	public TextMeshProUGUI scoreText;

	private void Awake()
	{
		if (_instance != null && _instance != this)
		{
			Destroy(this.gameObject);
		}
		else
		{
			_instance = this;
		}
	}


	void spawnOptioner(int countEnemy, int Uniques)
	{

		for (int i = 0; i < Uniques; i++)
		{
			EnemyController enemy = Instantiate(enemies[Random.Range(0, 2)], spawnPoints[Random.Range(0, 4)].transform.position, transform.rotation);
			enemy.isUnique = true;
		}

		for (int i = 0; i < countEnemy - Uniques; i++)
		{
			Instantiate(enemies[Random.Range(0, 2)], spawnPoints[Random.Range(0, 4)].transform.position, transform.rotation);
		}
	}


	private void Start()
	{
		spawnOptioner(totalEnemyCount, howManyUniques);
		StartCoroutine("BeginBattle");
	}

	void Update()
	{
		if(totalEnemyCount == deathEnemies)
		{
			Debug.Log("YOU WIN");
			youWinText.gameObject.SetActive(true);
			StartCoroutine(RestartGame());
		}
	}

	IEnumerator BeginBattle()
	{
		youWinText.GetComponent<TextMeshProUGUI>().text = "Battle Begins";
		youWinText.gameObject.SetActive(true);
		yield return new WaitForSeconds(2);
		youWinText.gameObject.SetActive(false);
		youWinText.GetComponent<TextMeshProUGUI>().text = "You Win";

	}

	IEnumerator RestartGame()
	{
		yield return new WaitForSeconds(5);
		GunSelector.Instance.grenadeAmmo = 3;
		youWinText.gameObject.SetActive(false);
		PlayerController.Instance.Teleport();
		SceneManager.LoadScene(0);
	}

	
}
