using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mouseLook : MonoBehaviour
{
	public float mouseSens = 100f;
	public Transform playerBody;

	float xRotation;

	private void Start()
	{
		Cursor.lockState = CursorLockMode.Locked;
	}

	private void Update()
	{
		float mouseX = Input.GetAxis("Mouse X") * mouseSens * Time.deltaTime;
		float mouseY = Input.GetAxis("Mouse Y") * mouseSens * Time.deltaTime;

		xRotation -= mouseY;
		xRotation = Mathf.Clamp(xRotation, -90, 90);

		transform.localRotation = Quaternion.Euler(xRotation, 0, 0);
		playerBody.Rotate(Vector3.up * mouseX);
	}
}
