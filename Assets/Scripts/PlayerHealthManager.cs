using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealthManager : MonoBehaviour
{
    private static PlayerHealthManager _instance;

    public static PlayerHealthManager Instance { get { return _instance; } }

    public float playerHealth;
    // Start is called before the first frame update

    // Update is called once per frame
    void Update()
    {
        if (playerHealth <= 0)
		{
            Destroy(gameObject);
            //invoke YOU LOSE
		}
    }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
    }

    public void TakeDamage(float damage)
	{
        playerHealth = playerHealth - damage;
	}
}
