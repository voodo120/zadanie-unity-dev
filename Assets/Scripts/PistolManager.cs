using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PistolManager : MonoBehaviour
{
    public Camera myCamera;
    public bool isZoomed = false;
    void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            ToggleZoom(isZoomed);
        }
    }

    public void ToggleZoom(bool zoomed)
    {
        if (!zoomed)
        {
            myCamera.fieldOfView = 40;
            isZoomed = true;
        }
        else
        {
            myCamera.fieldOfView = 60;
            isZoomed = false;
        }
    }
}
