using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class damageController : MonoBehaviour
{
	public bool weaponSwitcher; // true for pistol, false for machinegun

	private void OnCollisionEnter(Collision collision)
	{
		if (collision.gameObject.tag == "Enemy")
		{
			if (!collision.gameObject.GetComponent<EnemyController>().isUnique)
			{
				Destroy(collision.gameObject);
				Destroy(gameObject);
			}
			else if (collision.gameObject.GetComponent<EnemyController>().isUnique && weaponSwitcher == true)
			{
				Destroy(collision.gameObject);
				Destroy(gameObject);
			}
		}
	}

	private void Start()
	{

		if (GunSelector.Instance.currentWeapon.choice == Weapon.Weapons.Pistol)
		{
			weaponSwitcher = true;
		}
		else
		{
			weaponSwitcher = false;
		}
		StartCoroutine(DestroyDelay());
	}

	IEnumerator DestroyDelay()
	{

		yield return new WaitForSeconds(3);

		Destroy(gameObject);
	}
}
